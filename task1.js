const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputFile = "input2.json";
const outputFile = "output2.json";

var output = {}

jsonfile.readFile(inputFile, function(err, body){
    console.log("loaded input file content", body);
    const {names} =body

    output.emails = [];
    for (var i=0; i<names.length; i++) {
      output.emails.push(names[i].split('').reverse().join('')+randomstring.generate(5)+'@gmail.com');
    }
    
    jsonfile.writeFile(outputFile, output, {spaces: 2}, function(err) {
      console.log("All done!");
    });
});